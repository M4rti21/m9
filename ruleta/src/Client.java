import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final Scanner sc = new Scanner(System.in);
    private static final String HOST = "127.0.0.1";
    //private static final String HOST = "10.1.86.25";
    private static final int PORT = 5000;
    public static void main(String[] args) throws Exception {
        Mitjonet m = new Mitjonet(new Socket(HOST, PORT));
        m.recieve(Bytes.WELCOME);
        m.send(Bytes.ACK);
        System.out.print("Enter nickname: ");
        m.send_str(sc.nextLine());
        byte res = m.recieve(); // ACK or NICKNAME_IN_USE
        while (res == Bytes.NICKNAME_IN_USE) {
            System.out.print("Enter nickname: ");
            m.send_str(sc.nextLine());
            res = m.recieve();
        }
        joinGame(m);
        m.close();
    }

    public static void joinGame(Mitjonet m) {
        byte res = m.recieve(); // YOURE_IN or ONGOING
        while (res == Bytes.ONGOING)
            res = m.recieve();
        if (res == Bytes.YOURE_IN) {
            m.send(Bytes.ACK);
            play(m);
        }
    }

    public static void play(Mitjonet m) {
        System.out.println("Game started");
        byte res = m.recieve(); // BULLET or NO_BULLET
        while (res == Bytes.NO_BULLET)
            res = m.recieve();
        if (res == Bytes.WINNER) {
            System.out.println("You won!");
        } else {
            System.out.println("You lost!");
            System.out.println("Waiting for match to end...");
        }
        m.receivePlayers();
        System.out.println("Game ended");
        System.out.println("Do you want to play again? (y/n)");
        String input = sc.nextLine();
        if (input.equals("y")) {
            m.send(Bytes.CONTINUE);
            joinGame(m);
        } else {
            m.send(Bytes.END);
        }
    }
}
