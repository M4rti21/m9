public class Bytes {
    public static final byte ACK = 0b00000001;
    public static final byte WELCOME = 0b00000010;
    public static final byte NICKNAME_IN_USE = 0b00000011;
    public static final byte YOURE_IN = 0b00000100;
    public static final byte ONGOING = 0b00000101;
    public static final byte NO_BULLET = 0b00000110;
    public static final byte BULLET = 0b00000111;
    public static final byte WINNER = 0b00001000;
    public static final byte CONTINUE = 0b00001001;
    public static final byte END = 0b00001010;
    public static final byte UNKNOWN = 0b00000000;

    public static String str(byte b) {
        return switch (b) {
            case ACK -> "ACK";
            case WELCOME -> "WELCOME";
            case NICKNAME_IN_USE -> "NICKNAME_IN_USE";
            case YOURE_IN -> "YOURE_IN";
            case ONGOING -> "ONGOING";
            case NO_BULLET -> "NO_BULLET";
            case BULLET -> "BULLET";
            case WINNER -> "WINNER";
            case CONTINUE -> "CONTINUE";
            case END -> "END";
            default -> "UNKNOWN";
        };
    }

}
