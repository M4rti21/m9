import java.io.*;
import java.net.Socket;
import java.util.List;

import static java.lang.Thread.sleep;

public class Mitjonet {

    DataInputStream in;
    DataOutputStream out;
    ObjectOutputStream outObj;
    ObjectInputStream inObj;
    PrintWriter outStr;
    BufferedReader inStr;

    public Mitjonet(Socket socket) {
        super();
        try {
            this.out = new DataOutputStream(socket.getOutputStream());
            this.in = new DataInputStream(socket.getInputStream());
            this.outObj = new ObjectOutputStream(socket.getOutputStream());
            this.inObj = new ObjectInputStream(socket.getInputStream());
            this.outStr = new PrintWriter(socket.getOutputStream(), true);
            this.inStr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send(byte msg) {
        System.out.println("Sending: " + Bytes.str(msg));
        try {
            out.write(msg);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void send(List<String> msg) {
        System.out.println("Sending: " + msg);
        try {
            outObj.writeObject(msg);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void send_str(String msg) {
        System.out.println("Sending: " + msg);
        try {
            outStr.println(msg);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void receivePlayers() {
        try {
            Object res = inObj.readObject();
            System.out.println("Recieved: " + res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte recieve() {
        try {
            byte res = in.readByte();
            System.out.println("Recieved: " + Bytes.str(res));
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return Bytes.UNKNOWN;
        }
    }

    public byte recieve_timeout(int ms) throws Exception {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < ms) {
            if (in.available() > 0) {
                byte res = recieve();
                System.out.println("Recieved: " + Bytes.str(res));
                return res;
            }
        }
        throw new RuntimeException("Client timed out after " + ms + "ms");
    }
    public void recieve_timeout(byte expected, int ms) throws Exception {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < ms) {
            if (in.available() > 0) {
                byte res = recieve();
                System.out.println("Recieved: " + Bytes.str(res));
                if (res != expected) {
                    throw new RuntimeException("Expected " + Bytes.str(expected) + " but got " + Bytes.str(res));
                }
                return;
            }
        }
        throw new RuntimeException("Client timed out after " + ms + "ms");
    }

    public String receive_str_timeout(int ms) throws Exception {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < ms) {
            if (in.available() > 0) {
                String res = inStr.readLine();
                System.out.println("Receive: " + res);
                return res;
            }
        }
        throw new RuntimeException("Client timed out after " + ms + "ms");
    }

    public String receive_str() {
        try {
            String res = inStr.readLine();
            System.out.println("Receive: " + res);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void recieve(byte expected) {
        byte res = recieve();
        if (res != expected) {
            throw new RuntimeException("Expected " + Bytes.str(expected) + " but got " + Bytes.str(res));
        }
    }

    public void close() {
        try {
            out.close();
            in.close();
            outObj.close();
            inObj.close();
            outStr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
