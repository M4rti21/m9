import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	public static final int port = 5000;

	public static final ExecutorService pool = Executors.newCachedThreadPool();
	public static final Game game = new Game();

	public static void main(String[] args) {
		try (ServerSocket srv_socket = new ServerSocket(port)) {
			pool.execute(game);
			while (true) {
				Socket socket = srv_socket.accept();
				ClientHandler ch = new ClientHandler(socket, game);
				pool.execute(ch);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Closing server");
		}
	}

}
