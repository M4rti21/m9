import java.net.Socket;

public class ClientHandler implements Runnable {

    private final Mitjonet m;
    private String nick = "";

    private final Game game;

    public ClientHandler(Socket s, Game g) {
        this.m = new Mitjonet(s);
        this.game = g;
    }

    @Override
    public void run() {
        try {
            m.send(Bytes.WELCOME);
            m.recieve_timeout(Bytes.ACK, 3000);
            this.nick = m.receive_str_timeout(20000);
            System.out.println("Recieved nickname: " + this.nick);
            while (game.nickInUse(this.nick)) {
                System.out.println("Nickname in use");
                m.send(Bytes.NICKNAME_IN_USE);
                this.nick = m.receive_str_timeout(20000);
            }
            m.send(Bytes.ACK);
            boolean playing = true;
            while (playing) {
                boolean joined = false;
                while (!joined) {
                    System.out.println(this.nick + ": Waiting for game to start");
                    if (tryJoinGame()) {
                        System.out.println(this.nick + ": Joined game");
                        m.send(Bytes.YOURE_IN);
                        m.recieve_timeout(Bytes.ACK, 3000);
                        joined = true;
                    } else {
                        System.out.println(this.nick + ": Couldn't join game");
                        m.send(Bytes.ONGOING);
                        try {
                            synchronized (game.game_start) {
                                System.out.println(this.nick + ": Waiting for next game to start...");
                                game.game_start.wait();
                            }
                        } catch (InterruptedException ignored) {
                        }
                    }
                }
                game();
                byte res = m.recieve_timeout(10000);
                if (res == Bytes.CONTINUE) {
                } else {
                    playing = false;
                }
            }
        } catch (Exception e) {
            System.out.println("ClientHandler error: " + e.getMessage());
        } finally {
            m.close();
        }
    }

    public void game() {
        boolean alive = true;
        while (alive) {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException ignored) {
                }
                if (game.losers.contains(this)) {
                    m.send(Bytes.BULLET);
                    alive = false;
                } else if (game.winners.contains(this)) {
                    m.send(Bytes.WINNER);
                    alive = false;
                } else {
                    m.send(Bytes.NO_BULLET);
                }
            }
        }
        synchronized (game.game_end) {
            try {
                game.game_end.wait();
            } catch (InterruptedException ignored) {
            }
            m.send(game.getNames());
        }
    }

    private boolean tryJoinGame() {
        return Server.game.join(this);
    }

    public String getNick() {
        return nick;
    }


}
