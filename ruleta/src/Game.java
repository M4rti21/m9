import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import static java.lang.Thread.sleep;

public class Game implements Runnable {

    private final int min_size = 2;
    private final int max_size = 6;
    private final int win_size = 1;
    private Semaphore sem = new Semaphore(max_size);
    public final List<ClientHandler> waiting = new ArrayList<>();
    public final List<ClientHandler> playing = new ArrayList<>();
    public final List<ClientHandler> losers = new ArrayList<>();
    public final List<ClientHandler> winners = new ArrayList<>();
    public final List<String> names = new ArrayList<>();

    public final Object game_start = new Object();
    public final Object game_end = new Object();

    @Override
    public void run() {
        while (true) {
            this.playing.clear();
            this.losers.clear();
            this.names.clear();
            this.sem = new Semaphore(max_size);
            try {
                sleep(1000);
            } catch (InterruptedException ignored) {
            }
            System.out.println("GAME: New match started");
            synchronized (game_start) {
                game_start.notifyAll();
            }
            System.out.println("GAME: Waiting for players to join...");
            while (this.sem.availablePermits() > max_size - min_size) {
                try {
                    wait();
                } catch (Exception ignored) {
                }
            }
            try {
                System.out.println("GAME: Enough players joined, starting match in 5 seconds");
                sleep(1000);
                System.out.println("4");
                sleep(1000);
                System.out.println("3");
                sleep(1000);
                System.out.println("2");
                sleep(1000);
                System.out.println("1");
                sleep(1000);
                sem.drainPermits();
            } catch (InterruptedException ignored) {
            }
            System.out.println("GAME: Match started");
            while (this.playing.size() > this.win_size) {
                boolean[] bullets = getBullets();
                for (int i = 0; i < this.playing.size(); i++) {
                    ClientHandler ch = this.playing.get(i);
                    System.out.println("GAME: " + ch.getNick() + " has a bullet: " + bullets[i]);
                    if (bullets[i]) {
                        this.losers.add(ch);
                        this.names.add(ch.getNick());
                    }
                    synchronized (ch) {
                        ch.notify();
                    }
                    try {
                        sleep(1000);
                    } catch (InterruptedException ignored) {
                    }
                }
                for (ClientHandler ch: this.losers) {
                    this.playing.remove(ch);
                }
                this.losers.clear();
            }
            for (ClientHandler ch : this.playing) {
                this.names.add(ch.getNick());
                this.winners.add(ch);
                synchronized (ch) {
                    ch.notify();
                }
            }
            try {
                System.out.println("GAME: Ending match");
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            synchronized (game_end) {
                game_end.notifyAll();
            }
            try {
                System.out.println("GAME: End of match, starting match in 5 seconds");
                sleep(1000);
                System.out.println("4");
                sleep(1000);
                System.out.println("3");
                sleep(1000);
                System.out.println("2");
                sleep(1000);
                System.out.println("1");
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private boolean[] getBullets() {
        boolean[] res = new boolean[this.playing.size()];
        res[(int) (Math.random() * res.length)] = true;
        return res;
    }

    public synchronized boolean nickInUse(String nick) {
        for (ClientHandler ch : this.waiting)
            if (ch.getNick().equals(nick))
                return true;
        for (ClientHandler ch : this.playing)
            if (ch.getNick().equals(nick))
                return true;
        for (ClientHandler ch : this.losers)
            if (ch.getNick().equals(nick))
                return true;
        for (ClientHandler ch : this.winners)
            if (ch.getNick().equals(nick))
                return true;
        return false;
    }

    public synchronized boolean join(ClientHandler ch) {
        if (!sem.tryAcquire()) {
            this.waiting.add(ch);
            return false;
        }
        this.playing.add(ch);
        this.waiting.remove(ch);
        return true;
    }

    public synchronized List<String> getNames() {
        return this.names;
    }

}