package sync2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class act2 {

	public static void main(String[] args) throws InterruptedException {
		Buff1 b = new Buff1();
		ExecutorService executor = Executors.newFixedThreadPool(4);
		executor.execute(new Productor(b));
		executor.execute(new Productor(b));
		executor.execute(new Productor(b));
		Thread.sleep(3000);
		executor.execute(new Consumidor(b));
		while (true) {
			Thread.sleep(15000);
			b.clear();
		}
	}
}
