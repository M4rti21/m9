package sync2;

import java.util.ArrayList;
import java.util.List;

public class Buff1 {
	private List<String> buffer;
	private int seguent;
	private boolean estaBuida;
	private boolean estaPlena;

	public void clear() {
		this.buffer.clear();
		this.seguent = 0;
		this.estaBuida = true;
		this.estaPlena = false;
		System.out.println("Netejant buffer");
	}
	
	public Buff1() {
		this.buffer = new ArrayList<String>();
		this.seguent = 0;
		this.estaBuida = true;
		this.estaPlena = false;
	}

	public synchronized void produir(String s) {
		while (this.estaPlena) {
			try {
				wait();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		buffer.add(s);
		seguent++;
		this.estaBuida = false;
		if (seguent == this.buffer.size()) {
			this.estaPlena = true;
		}
		System.out.println(buffer);
		notifyAll();
	}

	public synchronized String consumir() {
		while (this.estaBuida) {
			try {
				wait();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		seguent--;
		this.estaPlena = false;

		if (seguent == 0) {
			this.estaBuida = true;
		}
		notifyAll();
		return this.buffer.get(seguent);
	}

}
