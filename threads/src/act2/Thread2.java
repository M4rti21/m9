package act2;

public class Thread2 extends Thread {
	int sec;
	int nom;

	public Thread2(int n, int s) {
		this.nom = n;
		this.sec = s;
	}

	public void run() {
		System.out.println("Iniciat Thread " + nom);

		try {
			for (int i = 0; i < sec; i++) {
				System.out.println("Thread " + nom + ": Queden " + (sec - i) + " segons...");
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
