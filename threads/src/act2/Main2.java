package act2;

import java.util.ArrayList;
import java.util.Random;

public class Main2 {

	public static void main(String[] args) throws InterruptedException {
		Random r = new Random();
		ArrayList<Thread> arr = new ArrayList<Thread>();
		for (int i = 0; i < 10; i++) {
			Thread2 t = new Thread2(i, r.nextInt(0, 10));
			t.start();
			arr.add(t);
			Thread.sleep(500);
		}
		for (Thread thread : arr) {
			thread.join();
		}

		System.out.println("final act 2");
	}

}
