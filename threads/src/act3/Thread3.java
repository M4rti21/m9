package act3;

public class Thread3 extends Thread {
	int sec;
	
	public Thread3(int s) {
		this.sec = s;
	}
	
	public void run() {
		for (int i = 0; i < sec; i++) {
			try {
				System.out.println("Queden " + (sec - i) + " segons...");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
