package act3;

public class Runnable1 implements Runnable {
	int sec;
	int nom;
	int secLeft;

	public Runnable1(int n, int s) {
		this.nom = n;
		this.sec = s;
		this.secLeft = s;
	}

	@Override
	public void run() {

		System.out.println("Iniciat Runnable " + nom);

		try {
			for (int i = 0; i < sec; i++) {
				System.out.println("Runnable " + nom + ": Queden " + (sec - i) + " segons...");
				Thread.sleep(1000);
				secLeft--;
			}
			System.out.println("Runnable " + nom + " ACABAT");
		} catch (InterruptedException e) {
			System.out.println("Runnable" + nom + " INTERROMPUT, han faltat: " + secLeft + " segons!");
		}
		
	}

}
