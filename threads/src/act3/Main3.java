package act3;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main3 {

	public static void main(String[] args) {
		Random r = new Random();

		ExecutorService exs = Executors.newFixedThreadPool(3);
		for (int i = 0; i < 10; i++) {
			exs.execute(new Runnable1(i, r.nextInt(0, 10)));
		}
		exs.shutdown();
		int sec = 5;
		try {
			for (int i = sec; i >= 0; i--) {
				exs.awaitTermination(1, TimeUnit.SECONDS);
				System.out.println("All threads will end in " + i + " seconds");
			}
			exs.shutdownNow();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
