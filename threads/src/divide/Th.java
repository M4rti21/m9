package divide;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class Th implements Callable<List<Integer>> {
	private List<Integer> nums;
	private int max;
	private int min;
	
	public Th(List<Integer> list) {
		this.nums = list;
		this.max = Integer.MIN_VALUE;
		this.min = Integer.MAX_VALUE;
	}

	public List<Integer> call() {
		for (int i = 0; i < nums.size(); i++) {
			if (nums.get(i) < min) min = nums.get(i);
			if (nums.get(i) > max) max = nums.get(i);
		}
		return Arrays.asList(min, max);
	}

}
