package divide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class act2 {
	public static void main(String[] args) {
		int n = 1000000;
		int ths = 2;
		Random r = new Random();
		List<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			nums.add(r.nextInt(0, Integer.MAX_VALUE));
		}
		System.out.println("S'ha començat");
		long start = System.currentTimeMillis();
		List<Future<List<Integer>>> res = new ArrayList<Future<List<Integer>>>();
		ExecutorService exs = Executors.newFixedThreadPool(ths);
		for (int i = 0; i < ths; i++) {
			res.add(exs.submit(new Th(nums.subList(n / ths * i, n / ths * (i + 1)))));
		}
		exs.shutdown();
		try {
			List<Integer> results = new ArrayList<Integer>();
			for (int i = 0; i < res.size(); i++) {
				System.out.println("Th" + i + "_MIN: " + res.get(i).get().get(0));
				System.out.println("Th" + i + "_MAX: " + res.get(i).get().get(1));
				results.add(res.get(i).get().get(0));
				results.add(res.get(i).get().get(1));
			}
			results.sort(null);
			System.out.println("Main_MIN: " + results.get(0));
			System.out.println("Main_MAX: " + results.get(results.size() - 1));
			long end = System.currentTimeMillis();
			System.out.println("El programa ha tardat: " + (end - start) + "ms");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
