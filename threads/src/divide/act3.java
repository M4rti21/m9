package divide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class act3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Longitud de la array: ");
		int n = sc.nextInt();
		System.out.print("Numero de threads: ");
		int ths = sc.nextInt();
		
		sc.close();
		Random r = new Random();
		List<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			nums.add(r.nextInt(0, Integer.MAX_VALUE));
		}
		System.out.println("S'ha començat");
		long start = System.currentTimeMillis();
		List<Future<List<Integer>>> res = new ArrayList<Future<List<Integer>>>();
		ExecutorService exs = Executors.newFixedThreadPool(ths);
		for (int i = 0; i < ths; i++) {
			res.add(exs.submit(new Th(nums.subList(n / ths * i, n / ths * (i + 1)))));
		}
		exs.shutdown();
		try {
			int min_R = Integer.MAX_VALUE;
			int max_R = Integer.MIN_VALUE;
			for (int i = 0; i < res.size(); i++) {
				int min =  res.get(i).get().get(0);
				int max =  res.get(i).get().get(1);
				System.out.println("Th" + i + "_MIN: " + min);
				System.out.println("Th" + i + "_MAX: " + max);
				if (min < min_R) min_R = min;
				if (max > max_R) max_R = max;
			}
			System.out.println("Main_MIN: " + min_R);
			System.out.println("Main_MAX: " + max_R);
			long end = System.currentTimeMillis();
			System.out.println("El programa ha tardat: " + (end - start) + "ms");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
