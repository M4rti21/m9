package divide;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class act1 {
	public static void main(String[] args) {
		int n = 1000000;
		Random r = new Random();
		List<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			nums.add(r.nextInt(0, Integer.MAX_VALUE));
		}
		ExecutorService exs = Executors.newFixedThreadPool(1);
		Th th = new Th(nums);
		System.out.println("S'ha començat");
		long start = System.currentTimeMillis();
		Future<List<Integer>> res = exs.submit(th);
		try {
			System.out.println("MIN: " + res.get().get(0));
			System.out.println("MAX: " + res.get().get(1));
			long end = System.currentTimeMillis();
			System.out.println("El programa ha tardat: " + (end - start) + "ms");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
