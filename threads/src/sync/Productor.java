package sync;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Productor implements Runnable {
	private Buff1 buffer;
	private final List<String> items = new ArrayList<String>();

	public Productor(Buff1 b) {
		this.buffer = b;
		this.items.addAll(Arrays.asList("metacarpus", "lecama", "monatomicity", "discloses", "unbiblical",
				"crawleyroot", "outbreeds", "gramophonic", "mariou", "a"));
	}

	@Override
	public void run() {
		while (true) {
			try {
				Random r = new Random();
				String p = items.get(r.nextInt(0, items.size()));
				buffer.produir(p);
				System.out.println("Afegim " + p + " als productes");
				int sleep = r.nextInt(1000, 5001);
				Thread.sleep(sleep);
			} catch (Exception e) {

			}
		}
	}

}
