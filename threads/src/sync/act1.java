package sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class act1 {

	public static void main(String[] args) {
		Buff1 b = new Buff1(10);
		ExecutorService executor = Executors.newFixedThreadPool(4);
		executor.execute(new Productor(b));
		executor.execute(new Consumidor(b));
	}
}
