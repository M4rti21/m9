package music;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.Instrument;

public class TestMusic {
	public static void main(String[] args) {
		Note[] notesLead = { new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter), new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter), new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter), new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter), new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter), new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter), new Note(Note.Frequency.A3, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter), new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter), new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter), new Note(Note.Frequency.A5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter), new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter), new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter) };

		Note[] notesRythm = { new Note(Note.Frequency.G5, Note.Duration.quarter),
				new Note(Note.Frequency.A5, Note.Duration.quarter), new Note(Note.Frequency.B5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter), new Note(Note.Frequency.G6, Note.Duration.quarter),
				new Note(Note.Frequency.A6, Note.Duration.quarter), new Note(Note.Frequency.B6, Note.Duration.quarter),
				new Note(Note.Frequency.D6, Note.Duration.quarter), new Note(Note.Frequency.C7, Note.Duration.quarter),
				new Note(Note.Frequency.G6, Note.Duration.quarter), new Note(Note.Frequency.E6, Note.Duration.quarter),
				new Note(Note.Frequency.D6, Note.Duration.quarter), new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter), new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter), new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter), new Note(Note.Frequency.A5, Note.Duration.quarter),
				new Note(Note.Frequency.B5, Note.Duration.quarter), new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.E6, Note.Duration.quarter), new Note(Note.Frequency.A6, Note.Duration.quarter),
				new Note(Note.Frequency.E6, Note.Duration.quarter), new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.B5, Note.Duration.quarter), new Note(Note.Frequency.A5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter), new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter) };

//		Instrument[] instruments = MidiPlayer.getInstruments();
//		for (Instrument instrument : instruments)
//			System.out.println(instrument.getName());

		System.out.print("Digues el BPM: ");
		Scanner sc = new Scanner(System.in);
		int bpm = sc.nextInt();
		sc.close();
		Conductor c = new Conductor(bpm);
		List<Performer> ps = new ArrayList<Performer>();
		ps.add(new Performer(0, notesLead, c));
		ps.add(new Performer(1, notesRythm, c));
		ExecutorService ex = Executors.newFixedThreadPool(ps.size() + 1);
		for (Performer p : ps) {
			ex.submit(p);
		}
		ex.execute(c);
		ex.shutdown();
		
	}

}
