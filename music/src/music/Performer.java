package music;

import java.util.concurrent.Callable;

public class Performer implements Callable<Boolean> {

	Conductor c;
	Note[] notes;
	int tmp;
	int ind;
	int id;

	public Performer(int id, Note[] n, Conductor t) {
		this.id = id;
		this.notes = n;
		this.tmp = 0;
		this.ind = 0;
		this.c = t;
	}

	@Override
	public Boolean call() throws Exception {
		synchronized (this.c) {
			c.wait();
		}
		for (Note note : notes) {
			MidiPlayer.play(this.id, note);
			for (int i = 0; i < note.getDuration(); i++) {
				synchronized (this.c) {
					c.wait();
				}
			}
			MidiPlayer.stop(this.id, note);
		}
		return true;
	}

}
