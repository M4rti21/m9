package music;

public class Conductor implements Runnable {

	int bpm;

	public Conductor(int bpm) {
		setBpm(bpm);
	}

	public void setBpm(int b) {
		this.bpm = b;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep((1000 / (this.bpm / 60)) / 16);
				System.out.println("tick");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (this) {
				notifyAll();
			}
		}
	}

}
