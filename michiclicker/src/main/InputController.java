package main;

import java.util.Scanner;

public class InputController implements Runnable {
    Scanner sc = new Scanner(System.in);

    private void startAction(String action) {
        switch (action) {
            case "0":
                GameController.quitGame();
                return;
            case "1":
                GameController.action1();
                return;
            case "2":
                GameController.action2();
                return;
            case "3":
                GameController.action3();
                return;
            case "4":
                GameController.action4();
                return;
            default:
                GameController.incrementMichitokens(1);
                return;
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                String r = sc.nextLine().trim();
                startAction(r);
            }
        } catch (Exception e) {
            System.err.println(e);

        }
    }

}
