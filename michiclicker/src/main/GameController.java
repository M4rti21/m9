package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import michis.Michibi;
import michis.Michihonk;
import michis.Michilover;
import michis.Michismart;

public class GameController {
    private static ExecutorService e = Executors.newCachedThreadPool();
    private static InputController i = new InputController();

    public static CountDownLatch c = new CountDownLatch(5);
    public static Semaphore s = new Semaphore(2);

    
    private static int tokens = 0;
    public static List<Michibi> michibis = new ArrayList<Michibi>();
    public static List<Michihonk> michihonks = new ArrayList<Michihonk>();
    public static List<Michilover> michilovers = new ArrayList<Michilover>();
    public static List<Michismart> michismarts = new ArrayList<Michismart>();

    private static List<Integer> keys = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5));
    public static boolean gameon = true;
    public static void main(String[] args) throws Exception {
        e.submit(i);
        printGUI();
    }

    public static List<Integer> getKeys() {
        return keys;
    }

    private static synchronized void printGUI() {
        System.out.println("Michitokens: " + tokens);
        System.out.println("Michibis: " + michibis.size());
        System.out.println("Michihonks: " + michihonks.size());
        System.out.println("Michilovers:" + michilovers.size());
        System.out.println("Michismarts: " + michismarts.size());
        System.out.println();
        System.out.println("""
                Choose an option:
                    Store:
                        1 - Michibi;
                        2 - Michihonk;
                        3 - Michilover;
                        4 - Michismart;
                    
                    Actions:
                        5 - Click!
                        0 - Exit;
                    """);
    }

    public static void action1() {
        int cost = 5;
        if (tokens - cost < 0) {
            System.out.println("Cant buy Michibi");
            return;
        }
        incrementMichitokens(-cost);
        Michibi m = new Michibi();
        e.submit(m);
        michibis.add(m);
    }

    public static void action2() {
        int cost = 20;
        if (tokens - cost < 0) {
            System.out.println("Cant buy Michihonk");
            return;
        }
        incrementMichitokens(-cost);
        Michihonk m = new Michihonk();
        e.submit(m);
        michihonks.add(m);
    }

    public static void action3() {
        int cost = 10;
        if (tokens - cost < 0) {
            System.out.println("Cant buy Michilover");
            return;
        }
        incrementMichitokens(-cost);
        Michilover m = new Michilover();
        if (c.getCount() == 0) {
            c = new CountDownLatch(5);
        }
        c.countDown();
        e.submit(m);
        michilovers.add(m);
    }

    public static void action4() {
        int cost = 100;
        if (tokens - cost < 0) {
            System.out.println("Cant buy Michismart");
            return;
        }
        incrementMichitokens(-cost);
        Michismart m = new Michismart(s);
        e.submit(m);
        michismarts.add(m);
    }

    public static synchronized void incrementMichitokens(int t) {
        tokens += t;
        printGUI();
    }

    public static void quitGame() {
        gameon = false;
        try {
            if (e.awaitTermination(1, TimeUnit.SECONDS)) {
                System.out.println("task completed");
            } else {
                System.out.println("Forcing shutdown...");
                e.shutdownNow();
            }
        } catch (InterruptedException e) {
        }
        System.out.println("Game finished");
    }
}
