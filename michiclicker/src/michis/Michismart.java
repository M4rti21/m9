package michis;

import java.util.concurrent.Semaphore;

import main.GameController;

public class Michismart implements Runnable {
    Semaphore s;

    public Michismart(Semaphore s) {
        super();
        this.s = s;
    }

    @Override
    public void run() {
        try {
            s.acquire();
            Thread.sleep(10000);
            GameController.incrementMichitokens(1000);
            s.release();
        } catch (InterruptedException e) {
        }
    }
}