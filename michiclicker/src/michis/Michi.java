package michis;

import main.GameController;

public abstract class Michi implements Runnable {

    int cost;
    int tokens;
    int rate_ms;

    public Michi(int cost, int tokens, int rate_sec) {
        this.cost = cost;
        this.tokens = tokens;
        this.rate_ms = rate_sec;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(rate_ms);
                GameController.incrementMichitokens(tokens);
            }
        } catch (InterruptedException e) {
        }

    }

}
