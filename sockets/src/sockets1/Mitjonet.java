package sockets1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import exceptions.ProtocolException;

@SuppressWarnings("unused")
public class Mitjonet {
	private Socket socket;
	private boolean verbose;
	private PrintWriter out;
	BufferedReader in_socket;
	BufferedReader in_console;

	private void send(String msg) {
		System.out.println("Sending: " + msg);
		out.println(msg);
	}

	private String recieve() {
		try {
			return in_socket.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public String send_recieve(String msg) {
		send(msg);
		return recieve();
	}

	public void send_recieve(String msg, String await_res) {
		send(msg);
		try {
			String res = recieve();
			if (!res.equals(await_res)) {
				throw new ProtocolException(await_res, res);
			}
		} catch(ProtocolException e) {
			e.printStackTrace();
		}
	}

	public Mitjonet(Socket socket) {
		super();
		init(socket, true);
	}

	public Mitjonet(Socket socket, boolean verbose) {
		super();
		init(socket, verbose);
	}

	private void init(Socket socket, boolean verbose) {
		try {
			this.socket = socket;
			this.out = new PrintWriter(socket.getOutputStream(), verbose);
			this.in_socket = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.in_console = new BufferedReader(new InputStreamReader(System.in));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
