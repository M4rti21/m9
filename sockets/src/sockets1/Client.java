package sockets1;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String[] args) throws UnknownHostException, IOException {
		final String hostname = "127.0.0.1";
		final int port  = 5000;
		Mitjonet m = new Mitjonet(new Socket(hostname, port));
		m.send_recieve("OH HI");
		m.send_recieve("BBYE");
		m.close();
	}
}
