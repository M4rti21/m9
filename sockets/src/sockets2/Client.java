import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static void main(String[] args) throws UnknownHostException, IOException {
		final String hostname = "127.0.0.1";
		final int port = 5000;
		Mitjonet m = new Mitjonet(new Socket(hostname, port));
		System.out.println("OH HI");
		opt_menu();
		String option = m.read();
		while (!option.equals("2")) {
			log(m.send_from_console());
			opt_menu();
			option = m.read();
		}
		log(m.send_recieve("BBYE"));
		m.close();
	}

	public static void log(String s) {
		System.out.println("Server: " + s);
	}

	public static void opt_menu() {
		System.out.println("""
				CHOOSE AN OPTION:
					1 - Send message
					2 - Close
				""");
	}
}
