import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		final int port = 5000;
		try {
			ServerSocket srv_socket = new ServerSocket(port);
			while (true) {
				Socket client_socket = srv_socket.accept();
				System.out.println("WELCOME");
				PrintWriter out = new PrintWriter(client_socket.getOutputStream(), true);
				BufferedReader inSock = new BufferedReader(new InputStreamReader(client_socket.getInputStream()));

				String msg = inSock.readLine();
				while (msg != null && !msg.equals("BBYE")) {
					System.out.println("Recieved: " + msg);
					System.out.println("Response: " + msg);
					out.println(msg);
					msg = inSock.readLine();
				}
				System.out.println("Recieved: " + msg);
				System.out.println("Response: KTHXBYE");
				out.println("KTHXBYE");
				client_socket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
