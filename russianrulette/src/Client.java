import java.io.IOException;
import java.net.Socket;

public class Client {

    static final String hostname = "10.1.86.25";
    static final int port = 60047;
    static Mitjonet m;

    public static void main(String[] args) throws IOException {
        m = new Mitjonet(new Socket(hostname, port));
        m.recieve_send("CYKA BLYATT", "ACK");
        do {
            m.recieve_send("QUIERO JUGAR A UN JUEGO", "ACK");
            int[] bullets = m.recieve_array();
            m.send("ACK");
            m.send(play_game(bullets));
            m.recieve(); //ACK
        } while (menu());
        m.close();
    }

    public static boolean menu() {
        m.recieve();
        int res;
        System.out.println("1: seguir jugant");
        System.out.println("2: acabar partida");
        res = m.scanner_int();
        if (res == 1) {
            m.send("EL QUE LA SACA LA USA");
            return true;
        } else {
            m.send("SOY UN PARGUELA");
            return false;
        }
    }

    public static String play_game(int[] bullets) {
        int rnd = 1;
        boolean dead = false;
        for (int i = 0; i < bullets.length - 1; i++) {
            if (i % 2 == 0 && bullets[i] == 1) {
                dead = true;
                break;
            }
            rnd++;
        }

        if (dead) {
            return "MUELTO VIVO " + rnd;
        }
        return "VIVO " + rnd;
    }

}
