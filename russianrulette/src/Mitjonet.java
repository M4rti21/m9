import java.io.*;
import java.net.Socket;
import java.util.Arrays;

@SuppressWarnings("unused")
public class Mitjonet {
    public Socket socket;
    public PrintWriter out;
    public BufferedReader in;
    public BufferedReader cmd_in;
    private ObjectInputStream obj_in;
    private ObjectOutputStream obj_out;
    private DataInputStream data_in;
    private DataOutputStream data_out;

    public Mitjonet(Socket socket) {
        super();
        init(socket, true);
    }
    public void send(String msg) {
        out.println(msg);
        System.out.println("Client: " + msg);
    }

    public int scanner_int() {
        try {
            return Integer.parseInt(cmd_in.readLine().trim());
        } catch (Exception e) {
            return 0;
        }
    }

    public String recieve() {
        try {
            String str = in.readLine();
            System.out.println("Server: " + str);
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public int[] recieve_array() {
        int[] str;
        try {
            str = (int[]) obj_in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Server: " + Arrays.toString(str));
        return str;
    }

    public void close() {
        try {
            out.close();
            in.close();
            cmd_in.close();
            obj_out.close();
            obj_in.close();
            data_in.close();
            data_out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String recieve_send(String req, String res) {
        String resp = recieve();
        send(res);
        return res;
    }

    public String send_recieve(String req, String res) {
        send(req);
        String resp = recieve();
        if (!res.equals(req)) {
            try {
                throw new ProtocolException(resp, req);
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    private void init(Socket socket, boolean verbose) {
        try {
            this.socket = socket;
            this.out = new PrintWriter(socket.getOutputStream(), verbose);
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.cmd_in = new BufferedReader(new InputStreamReader(System.in));
            this.data_out = new DataOutputStream(socket.getOutputStream());
            this.data_in = new DataInputStream(socket.getInputStream());
            this.obj_out = new ObjectOutputStream(socket.getOutputStream());
            this.obj_in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
