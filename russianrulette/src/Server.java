import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Server {

    public static void main(String[] args) {
        final int port = 5000;
        try {
            Random r = new Random();
            ServerSocket srv_socket = new ServerSocket(port);
            while (true) {
                try {
                    System.out.println("WAITING...");
                    Socket client_socket = srv_socket.accept();
                    PrintWriter out = new PrintWriter(client_socket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(client_socket.getInputStream()));
                    ObjectOutputStream obj_out = new ObjectOutputStream(client_socket.getOutputStream());
                    System.out.println("s'ha connectat");
                    out.println("CYKA BLYATT");
                    log(in.readLine(), "ACK");
                    boolean playing = true;
                    do {
                        out.println("QUIERO JUGAR A UN JUEGO");
                        log(in.readLine(), "ACK");
                        int[] bullets = new int[6];
                        bullets[r.nextInt(6)] = 1;
                        obj_out.writeObject(bullets);
                        log(in.readLine(), "ACK");
                        log(in.readLine(), "VIVO O MUERTO");
                        out.println("ACK");
                        out.println("NO HAY HUEVOS?");
                        if (log(in.readLine(), "SI o NO").equals("SOY UN PARGUELA")) {
                            playing = false;
                        }
                    } while (playing);
                    out.close();
                    in.close();
                    obj_out.close();
                    client_socket.close();
                } catch (Exception e) {
                    System.out.println("S'ha perdut conexio amb el client");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String log(String msg, String exp) {
        System.out.println("Client: " + msg);
        System.out.println("Expected: " + exp);
        return msg;
    }
}
