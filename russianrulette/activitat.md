# Activitat: Ruleta DAMVi

L’objectiu d’aquesta activitat és crear un parell d’aplicacions (client i servidor) amb les quals puguem jugar al joc de la ruleta com a client front a un servidor.


## Eines prèvies

La classe per a gestionar la comunicació que heu realitzat a la pràctica 0.


## Protocol de comunicació

Per tal que les nostres aplicacions es parlin, han de seguir una sèrie de protocols. En el cas del nostre trivial, aquest serà el següent:



* **Connectar-se:**
    * Servidor -> **CYKA BLYATT (Bienvenida)**
    * Client -> **ACK**
* **Iniciar partida:**
    * Servidor -> **QUIERO JUGAR A UN JUEGO**
    * Client -> **ACK**
    * **Enviament de pregunta i respostes:**
        * Servidor -> Envia array ruleta
        * Client -> **ACK**
    * En aquest punt, el client gestiona la partida de forma local.
    * **Rebuda de resposta del jugador:**
        * Client -> enviarà **VIVO #** o **MUELTO VIVO #**. # indica el nombre de jugades.
        * Servidor -> **ACK**
* **Seguir jugant o plegar:**
    * Servidor: **NO HAY HUEVOS? (O1 (Sí): EL QUE LA SACA LA USA | O2 (No): SOY UN PARGUELA)**
    * Client -> **O2 (No): SOY UN PARGUELA**
    * Servidor -> **KURWA**


## Pas 1

Implementa l’aplicació Ruleta DAMVi client que es connectarà a l’ordinador servidor de classe.

Aquest client l’array de la ruleta en aquesta partida. Acte seguit, farà la partida en local i s’anirà anotant el nombre de jugades que realitza. L’array és d'enters que només poden ser 0 o 1, només hi ha un 1.

Finalment, enviareu si sigueixes viu o no i el nombre de jugades que heu fet.

Per finalitzar, caldrà saber si voleu continuar jugant o plegar i seguir el protocol adientment.


## Pas 2

Implementa un servidor que segueixi el protocol i es connecti amb el teu client.

Una vegada heu fet el client funcional NO TOQUEU cap part del client corresponent a l’enviament de missatges.


## Pas 3

Interconnexió entre aplicacions:

Cal que les aplicacions es parlin entre elles (client i servidor).


## Qualificacions

50%: funciona

30%: sòlid -> s’adapta a errors del client a nivell d’input

20%: òptim -> s’adapta a errors de xarxa (servidor)


## Exemples de partida


<table>
  <tr>
   <td><strong>Client</strong>
   </td>
   <td><strong>Servidor</strong>
   </td>
   <td><strong>Events</strong>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>CYKA BLYATT
   </td>
   <td>envia missatge de benvinguda
   </td>
  </tr>
  <tr>
   <td>ACK
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>QUIERO JUGAR A UN JUEGO
   </td>
   <td>avisa de l’inici de partida
   </td>
  </tr>
  <tr>
   <td>ACK
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>“[0,0,0,1,0,0]”
   </td>
   <td>envia una array
   </td>
  </tr>
  <tr>
   <td>ACK
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>VIVO 5
   </td>
   <td>
   </td>
   <td>ha guanyat en 5 jugades
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>ACK
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>NO HAY HUEVOS?
   </td>
   <td>pregunta si es vol seguir jugant
   </td>
  </tr>
  <tr>
   <td>EL QUE LA SACA LA USA
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>QUIERO JUGAR A UN JUEGO
   </td>
   <td>avisa de l’inici de partida
   </td>
  </tr>
  <tr>
   <td>ACK
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>[1,0,0,0,0,0]
   </td>
   <td>envia una array
   </td>
  </tr>
  <tr>
   <td>ACK
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>MUELTO VIVO 1
   </td>
   <td>
   </td>
   <td>ha perdut en 1 jugada
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>ACK
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>NO HAY HUEVOS?
   </td>
   <td>pregunta si es vol seguir jugant
   </td>
  </tr>
  <tr>
   <td>SOY UN PARGUELA
   </td>
   <td>
   </td>
   <td>el client vol plegar
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>KURWA
   </td>
   <td>se li accepta el tancar
   </td>
  </tr>
</table>
