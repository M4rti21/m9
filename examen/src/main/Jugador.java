package main;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class Jugador implements Callable<JugadorStats> {

	Semaphore sp;
	CountDownLatch cdl;

	List<Arma> guns;

	private String nom;
	private int puntuacio;

	Random r;

	public Jugador(String nom, List<Arma> guns, Semaphore sp, CountDownLatch cdl) {
		super();
		this.sp = sp;
		this.cdl = cdl;
		this.guns = guns;
		this.r = new Random();

		this.nom = nom;
		this.puntuacio = 0;
	}

	public String getNom() {
		return nom;
	}

	public int getPuntuacio() {
		return puntuacio;
	}

	@Override
	public JugadorStats call() {
		try {
			sp.acquire();
			System.out.println(this.nom + " ha entrat al joc");
			boolean viu = true;
			while (viu) {
				if (cdl.getCount() == 1) {
					return new JugadorStats(nom, puntuacio, true);
				}
				Thread.sleep(r.nextInt(1000, 3000));
				switch (r.nextInt(3)) {
				case 0:
					System.out.println(this.nom + " ha MORT");
					viu = false;
					break;
				default:
					Arma gun = guns.get(r.nextInt(guns.size()));
					if (gun.equip()) {
						this.puntuacio += gun.getPunts();
						System.out.println(this.nom + " agafa " + gun.getNom());
					} else {
						this.puntuacio -= 10;
						System.out.println(
								this.nom + " ha intentat agafar " + gun.getNom() + " pero ha tingut un lapsus");
					}
					break;
				}
			}
			sp.release();
			cdl.countDown();
		} catch (InterruptedException e) {
		}
		return new JugadorStats(nom, puntuacio, false);
	}

}
