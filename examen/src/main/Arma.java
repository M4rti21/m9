package main;

import java.util.concurrent.CountDownLatch;

public class Arma {
	
	private String nom;
	private int punts;
	private CountDownLatch amount;
	
	public Arma(String nom, int punts, int amount) {
		super();
		this.nom = nom;
		this.punts = punts;
		this.amount = new CountDownLatch(amount);
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getPunts() {
		return punts;
	}
	
	public boolean equip() {
		amount.countDown();
		return amount.getCount() > 0;
	}
	
	public int getAmount() {
		return (int) amount.getCount();
	}

	@Override
	public String toString() {
		return nom + " -> amount left: " + this.getAmount();
	}
	
}
