package main;

public class JugadorStats {
	
	private String nom;
	private int puntuacio;
	private boolean is_alive;
	
	public JugadorStats(String nom, int puntuacio, boolean is_alive) {
		super();
		this.nom = nom;
		this.puntuacio = puntuacio;
		this.is_alive = is_alive;
	}
	
	public String getNom() {
		return nom;
	}

	public int getPuntuacio() {
		return puntuacio;
	}

	public boolean is_alive() {
		return is_alive;
	}

	@Override
	public String toString() {
		return nom + " -> " + puntuacio + "pts";
	}
	
}
