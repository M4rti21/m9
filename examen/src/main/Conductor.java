package main;

import java.util.concurrent.ExecutorService;

public class Conductor implements Runnable {
	private ExecutorService exs;
	private boolean is_mad;
	
	public Conductor(ExecutorService exs, boolean is_mad) {
		super();
		this.exs = exs;
		this.is_mad = is_mad;
	}

	@Override
	public void run() {
		if (is_mad) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
			}
			System.out.println("BOOOOOOOOOOOOOOOOOOOOMMMMMM!!!!!!!!!!!!!");
			exs.shutdownNow();
		}
	}
	
}
