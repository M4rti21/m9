package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;

public class Main {

	public static void main(String[] args) {

		int players_num = 15;
		int game_size = 5;

		Semaphore sp = new Semaphore(game_size);
		CountDownLatch cdl = new CountDownLatch(players_num);

		ExecutorService exs = Executors.newFixedThreadPool(game_size);
		exs.submit(new Conductor(exs, false));
		List<Future<JugadorStats>> ftrs = new ArrayList<Future<JugadorStats>>();

		List<Arma> guns = new ArrayList<Arma>();

		guns.add(new Arma("pistola", 50, 10));
		guns.add(new Arma("escopeta", 75, 7));
		guns.add(new Arma("ar", 100, 5));
		guns.add(new Arma("sniper", 200, 3));

		for (int i = 0; i < players_num; i++) {
			Jugador j = new Jugador("J" + i, guns, sp, cdl);
			ftrs.add(exs.submit(j));
		}

		exs.shutdown();
		try {
			List<JugadorStats> results = new ArrayList<JugadorStats>();
			for (int i = 0; i < players_num; i++) {
				JugadorStats p = ftrs.get(i).get();
				if (p.is_alive()) {
					System.out.println();
					System.out.println("<<<<< ULTIM VIU >>>>>");
					System.out.println(p.getNom() + " ha sigut el ultim viu");
				}
				results.add(p);
			}

			System.out.println();
			System.out.println("<<<<< RESULTS >>>>>");
			Comparator<JugadorStats> j_comp = (j1, j2) -> j2.getPuntuacio() - j1.getPuntuacio();
			results.sort(j_comp);
			for (JugadorStats j : results) {
				System.out.println(j);
			}
			System.out.println();
			System.out.println("<<<<< GUNS >>>>>");
			Comparator<Arma> g_comp = (a1, a2) -> a2.getAmount() - a1.getAmount();
			guns.sort(g_comp);
			for (Arma a : guns) {
				System.out.println(a);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
